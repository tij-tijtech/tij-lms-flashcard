var url="https://docs.google.com/spreadsheets/d/1hLuliCk2O9pGNbF1pObO650gqMtuNFKmyyoOgPDXW-k/edit#gid=1543702850";

function getScriptUrl() {
 var url = ScriptApp.getService().getUrl();
 console.log(url)
 return url;
}

function getData11(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("1課１");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData12(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("1課２");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData13(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("1課３");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData14(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("1課4");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData15(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("1課5");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData21(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("2課1");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData22(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("2課2");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData23(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("2課3");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData24(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("2課4");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}


function getData25(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("2課5");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData31(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("3課1");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData32(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("３課2");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData33(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("３課3");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData34(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("３課4");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData35(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("３課5");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}



function getData41(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("4課1");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData42(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("4課2");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData43(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("4課3");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData44(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("4課4");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData45(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("4課5");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}


function getData51(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("5課１ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData52(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("5課２ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData53(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("5課３ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData54(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("5課４ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData55(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("5課５ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}


function getData61(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("6課１ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData62(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("6課２ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData63(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("6課３ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData64(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("6課４ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData65(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("6課５ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}


function getData71(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("7課１  のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData72(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("7課２  のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData73(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("7課３  のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData74(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("7課４ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData75(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("7課５ のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}


function getData81(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("8課１  のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData82(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("8課2 のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData83(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("8課3 のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData84(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("8課4  のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function getData85(){
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var hirarr=[]
  var eiarr=[]
  var imgarr=[]
  var sndarr=[]
  
    myws=sh.getSheetByName("8課5  のコピー");
    
    var listKanji = myws.getRange(44,1,myws.getRange("A44").getDataRegion().getLastRow()-43,1).getValues();
    var listHira = myws.getRange(44,2,myws.getRange("B44").getDataRegion().getLastRow()-43,1).getValues();
    var listEigo = myws.getRange(44,3,myws.getRange("C44").getDataRegion().getLastRow()-43,1).getValues();
    var listImg = myws.getRange(44,4,myws.getRange("D44").getDataRegion().getLastRow()-43,1).getValues();
    var listSnd = myws.getRange(44,5,myws.getRange("E44").getDataRegion().getLastRow()-43,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    hirarr.push(listHira.map(function(r){return r[0];}))
    eiarr.push(listEigo.map(function(r){return r[0];}))
    imgarr.push(listImg.map(function(r){return r[0];}))
    sndarr.push(listSnd.map(function(r){return r[0];}))
    
   var coba ={Kanji:kanarr,Hira:hirarr,Eng:eiarr,Image:imgarr,Sound:sndarr}
   return coba;
}

function doGet(e) {
 
  if (!e.parameter.page) {
     return HtmlService.createTemplateFromFile('slider11').evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME); 
     
  }else{
    return HtmlService.createTemplateFromFile(e.parameter['page']).evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME);
  }
}

function include(filename){
  return HtmlService.createHtmlOutputFromFile(filename)
  .getContent();
}
